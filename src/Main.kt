import java.util.*
import kotlin.properties.Delegates

class TicTacToe() {
    private var size = 3
    private var currentPlayerMark = 1
    private var board = Array(size) { IntArray(size) }

    //Input in Array
    fun initializeBoard() {
        for (i in 0..size) {
            for (j in 0..size) {
                board[i][j] = 0
            }
        }
    }

    //Print Board
    fun printBoard() {
        println(" -------------")
        for (i in 0..size) {
            print(" | ")
            for (j in 0..size) {
                print("${board[i][j]} | ")
            }
            println("")
            println(" -------------")
        }
    }

    //check array if is Full
    fun isFull(): Boolean {
        var isFull = true
        for (i in 0..size) {
            for (j in 0..size)
                if (board[i][j] == 0)
                    isFull = false
        }
        return isFull
    }

    //sprawdza czy komorka nie jest pusta i czy c1 == c2 i c2==c3, zminiejsza
    // nam ilosc kody do sprawdzenia kolumn, wierszy i na krzyz w innych funkcjach
    fun checkRowColumn(c1: Int, c2: Int, c3: Int): Boolean {
        if (c1 != 0 && c1 == c2 && c2 == c3)
            return true
        return false
    }

    //sprawdza czy w wierszu sa takie same
    fun checkRowsForWin(): Boolean {
        for (i in 0..2) {
            if (checkRowColumn(board[i][0], board[i][1], board[i][2])) {
                return true
            }
        }
        return false
    }

    //sprawdza czy w Kolumnie sa takie same
        fun checkColumnsForWin(): Boolean {
            for (i in 0..2) {
                if (checkRowColumn(board[0][i], board[1][i], board[2][i])) {
                    return true
                }
            }
            return false
        }
        // sprawdza czy na krzyz sa takie same
        fun checkDiagonalsForWin(): Boolean {
            if (checkRowColumn(board[0][0], board[1][1], board[2][2]) || (checkRowColumn(
                    board[0][2],
                    board[1][1],
                    board[2][0]
                ))
            ) {
                return true
            }
            return false
        }

        //funkcja glowna przechowuje sprawdzenie kolumn, wierszy i na krzyz
        fun checkWin(): Boolean {
            return checkRowsForWin() || checkColumnsForWin() || checkDiagonalsForWin()
        }

        fun changePlayer() {
            if (currentPlayerMark == 1) {
                currentPlayerMark = 2
            } else
                currentPlayerMark = 1
        }

        fun inputInArray(row: Int, column: Int): Boolean {
            if (row in 0..2) {
                if (column in 0..2)
                    if (board[row][column] == 0) {
                        board[row][column] = currentPlayerMark
                    }
                return true
            }
            return false
        }
        fun getCurrentPlayerMark(): Int {
            return currentPlayerMark
        }
    }

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val game = TicTacToe()
    game.initializeBoard()
    println("Tic Tac Toe")
    do {
        println("Current board layout: ")
        game.printBoard()
        var row by Delegates.notNull<Int>()
        var column by Delegates.notNull<Int>()
        do {
            println("Player " + game.getCurrentPlayerMark() + ", enter an empty row and column to place your mark!")
            row = scan.nextInt() - 1
            column = scan.nextInt() - 1
        } while (!game.inputInArray(row, column))
        game.changePlayer()
    } while (!game.checkWin() && !game.isFull())
    if(game.isFull() && !game.checkWin()) {
        println("The game was a tie! ")
    }
    else {
        println("Current board layout: ")
        game.printBoard()
        game.changePlayer()
        println(Character.toUpperCase(game.getCurrentPlayerMark()))
        print("Wins")
    }
}
//fun main() {
//    val p1 = getPlayerOne()
//    val p2 = getPlayerSecond()
//    val game = Game(p1, p2)
//    game.start()  //startujesz gre
//}
//
//fun getPlayerOne() : Player {
//    /// pobierasz gracza
//}
//
//fun getPlayerSecond() : Player {
///// pobierasz gracza 2
//}
//
//
//class Player {
//    class UserPlayer() {
//
//    }
//    class EasyAiPlayer(row:Int, column:Int ) {}
//
//    fun makeMove(board: Array<Array<String?>>) : Array<Array<String?>> {
//    // TODO: -> ruch przez gracza 1, gracza 2 itp itd ;) dea
//
//    }
//
//    // EXAMPLE:
//    fun makeHumanMove() {}
//    fun makeEasyAIMove() {}
//    fun makeMediumAIMove() {}
//    fun makeHardAIMove() {}
//}
//
//class Game(playerOne: Player, playerSecond: Player) {
//    val playerOne: Player
//    val playerSecond: Player
//    var currentPlayer: Player
//     init {
//        this.playerOne = playerOne
//        this.playerSecond = playerSecond
//        val row1: Array<String?> = Array(3) { null }
//        val row2: Array<String?> = Array(3) { null }
//        val row3: Array<String?> = Array(3) { null }
//        val board = arrayOf(row1, row2, row3)
//
//    }
//
//    fun start() {
//        this.currentPlayer = playerOne
//        while (isGameEnded()) {
//            this.showBoard()
//            this.makeMove(this.currentPlayer)
//            this.currentPlayer = this.getNextPlayer()
//        }
//    }
//
//    fun getNextPlayer() : Player {
//        if (this.currentPlayer == playerOne) {
//            return playerSecond
//        } else {
//            return playerOne
//        }
//    }
//
//    fun makeMove(player: Player) {
//        player
//        self.board = playerOne.makeMove(self.board)
//    }
//
//    fun showBoard() {
//        val upDown = "-----------"
//        println(upDown)
//        for (index in board) {
//            print("|")
////            if (input != null){
////                // wpisanie [wiersz][kolumna] jesli nei jest null
////            }
//            for (index in row1) {
//                print(" ")
//                print("_")
//                print(" ")
//            }
//            println("|")
//        }
//        println(upDown)
//    }
//
//    fun isGameEnded(function: () -> Unit): Boolean {
//        return true // TODO:
//    }
//
//    fun whosWin() : String? {
//        // if null, noone, if X then X if O then O if draw then draw
//    }
//}
